#Mise en forme de l'instance du problème

#A partir d'une liste de n points contenant les coordonnées sous la forme de couple (abscisse, ordonnée), la fonction calcule_distances renvoie la matrice d'adjacence du graphe associé.

def distance(a,b):
    #Fonction auxiliaire
    #a et b sont des couples représentant des coordonnées
    #Renvoie la distance entre les deux points
    xa,ya=a
    xb,yb=b
    return ((yb-ya)**2+(xb-xa)**2)**0.5

#O(1)

def calcule_distances(P):
    #P est une liste de coordonnées
    #Renvoie la matrice d'adjacence du graphe associé : D[i][j] contient la distance entre le sommet i et le sommet j
    n=len(P)
    D=[]
    for i in range(n):
        L=[]
        for j in range(n):
            L.append(distance(P[i],P[j]))
        D.append(L)
    return D

#O(n²)

#-------------------------------------------------------------------------------------------------------------------------------

#Algorithme naïf

def longueur_chemin(D,c):
    #Fonction auxiliaire
    #D représente la matrice d'adjacence du graphe, c un chemin sous la forme d'un tuple constitué des indices i des sommets, sans faire figurer 0 au début et à la fin pour plus de facilités dans l'algorithme
    n=len(D)
    s=D[0][c[0]]
    for i in range(n-2):
        s+=D[c[i]][c[i+1]]
    s+=D[c[-1]][0]
    return s

#O(n)

from itertools import permutations

def resolution_tsp_naive(D):
    #D représente la matrice d'adjacence du graphe
    #Renvoie  le cycle hamiltonien de plus petit poids sous la forme d'un tuple (ou d'une liste si le cycle choisi au début est minimisant) constitué des indices i des sommets, sans faire figurer 0 au début et à la fin
    n=len(D)
    c=[x for x in range(1,n)]  #cycle minimisant temporaire
    m=longueur_chemin(D,c)     #valeur du minimum temporaire
    for x in permutations(c):
        l=longueur_chemin(D,x)
        if l<m:
            m,c=l,x
    return c

#O(n!)

def sol_tsp_algo_naif(D):
    #D représente la matrice d'adjacence du graphe
    #Renvoie la solution de l'algorithme naïf sous la forme attendue (liste des indices des sommets du cycle, le sommet 0, figurant au début, n'est pas indiqué en fin de liste)
    c=resolution_tsp_naive(D)
    L=[0]
    for i in range(len(c)):
        L.append(c[i])
    return L

#O(n!)

#-------------------------------------------------------------------------------------------------------------------------------

#Programmation dynamique

import math

#Pour pouvoir accéder facilement aux différents ensembles que nous allons manipuler, nous allons exploiter la structure de dictionnaire

def listetaille(t,n):
    #Création d'une liste qui contient les ensembles de taille t contenant 0 (sommet de départ) sous forme de listes triées. Les sommets sont compris entre 0 et n-1 inclus. On suppose t<=n
    tot=math.comb(n-1,t-1)    #nombre d'ensembles de taille t
    L=[]
    L0=[]
    for i in range(0,t):  #création de la liste initiale
        L0.append(i)
    Lc0=L0[:]
    L.append(Lc0)
    nb=1                  #nombre d'ensembles déjà créés
    while nb!=tot:
        if L0[-1]<n-1:
            L0[-1]+=1
            Lc=L0[:]
            L.append(Lc)
            nb+=1
        else:
            j=t-1
            while L0[j-1]==L0[j]-1:
                j-=1
            j-=1
            L0[j]+=1
            for i in range (j+1,t):
                L0[i]=L0[i-1]+1
            Lc=L0[:]
            L.append(Lc)
            nb+=1
    return L

# ex : listetaille(4,6) :
# [[0, 1, 2, 3],
#  [0, 1, 2, 4],
#  [0, 1, 2, 5],
#  [0, 1, 3, 4],
#  [0, 1, 3, 5],
#  [0, 1, 4, 5],
#  [0, 2, 3, 4],
#  [0, 2, 3, 5],
#  [0, 2, 4, 5],
#  [0, 3, 4, 5]]

#O(tot*t)

def listedico(n):
    #Les sommets sont compris entre 0 et n-1
    #Renvoie un couple constitué d'une liste de dictionnaires et du nombre total d'ensembles. Les dictionnaires contiennent les ensembles de taille i avec i l'indice de la liste
    D=[{}]
    c=0
    for t in range(1,n+1):
        L=listetaille(t,n)
        d={}
        for i in range(0,len(L)):
            d[c]=L[i]
            c+=1
        D.append(d)
    return D,c

#ex : listedico(4):
# ([{}, {0: [0]}, {1: [0, 1], 2: [0, 2], 3: [0, 3]}, {4: [0, 1, 2], 5: [0, 1, 3], 6: [0, 2, 3]}, {7: [0, 1, 2, 3]}], 8)

#O(n*2^n)

def dico_inverse(n):
    #Construit la liste de dictionnaires "inverse" du précédent : les clés sont les ensembles sous la forme de tuple, et les valeurs correspondent aux clés du dictionnaire précédent
    D=[{}]
    v=0
    for t in range(1,n+1):
        L=listetaille(t,n)
        d={}
        for i in range(0,len(L)):
            d[tuple(L[i])]=v
            v+=1
        D.append(d)
    return D

#ex : dico_inverse(4):
#[{}, {(0,): 0}, {(0, 1): 1, (0, 2): 2, (0, 3): 3}, {(0, 1, 2): 4, (0, 1, 3): 5, (0, 2, 3): 6}, {(0, 1, 2, 3): 7}]

#O(n*2^n)

def TSPdyn(Dist):
    #Dist est un graphe à n sommets, numéroté de 0 à n-1, sous forme de matrice d’adjacence
    #Renvoie le poids du cycle hamiltonien de poids minimal, le prédécesseur de 0 dans ce chemin et le tableau des prédécesseurs
    n=len(Dist)
    D,nbens=listedico(n)
    D_inv=dico_inverse(n)
    delta=[[float('inf')]*n for _ in range (nbens)]  #initialisation des tableaux indexés par les ss-ens et les sommets 0, 1,...,n-1
    pred=[[0]*n for _ in range (nbens)]
    for i in range(0,n):
        delta[i][i]=Dist[0][i]
    for t in range(2,n+1):
        for cle,ens in D[t].items():
            for j in ens:
                if j!=0:
                    ssens=ens[:]
                    ssens.remove(j)
                    tuple_ssens=tuple(ssens)
                    sscle=D_inv[t-1][tuple_ssens]     #clé associée au ss-ens ens\{j}
                    min=delta[sscle][ssens[-1]]+Dist[ssens[-1]][j]   #initialisation arbitraire du min
                    imin=ssens[-1]
                    for im in ssens:
                        if delta[sscle][im]+Dist[im][j]<min:
                            min=delta[sscle][im]+Dist[im][j]
                            imin=im
                    delta[cle][j]=min
                    pred[cle][j]=imin
    jmini=1
    mini=delta[-1][1]+Dist[1][0]        #initialisation arbitraire de mini
    for j in range(2,n):
        if delta[-1][j]+Dist[j][0]<mini:
            mini=delta[-1][j]+Dist[j][0]
            jmini=j
    return mini,jmini,pred       #mini est la distance minimale, jmini est le prédécesseur de 0 dans le chemin associé

#O(n²*2^n)

def sol_tsp_prog_dyn(Dist):
    #Dist représente la matrice d'adjacence du graphe
    #Renvoie la solution calculée via l'algorithme d'Held and Karp
    C=[0]
    m,j,pred=TSPdyn(Dist)
    C.append(j)
    n=len(Dist)
    D,nbens=listedico(n)
    D_inv=dico_inverse(n)
    cle=nbens-1      #il s'agit de la clé associée à l'ensemble constituée de tous les sommets
    ens=D[-1][cle]
    t=n
    while t>2:
        p=C[-1]
        C.append(pred[cle][p])
        ens.remove(p)
        t-=1
        cle=D_inv[t][tuple(ens)]
    return C

#O(n²*2^n)

#-------------------------------------------------------------------------------------------------------------------------------

#Algorithme du plus proche voisin

def sol_tsp_algo_plus_proche_voisin(D):
    #D représente la matrice d'adjacence du graphe
    #Renvoie la solution calculée via l'algorithme du plus proche voisin
    n=len(D)
    dispo=[True]*n
    dispo[0]=False
    c=[0]
    prec=0
    for i in range(n-1):
        d,m=float('inf'),0
        for j in range(1,n):
            if dispo[j] and D[prec][j]<d:
                d=D[prec][j]
                m=j
        c.append(m)
        dispo[m]=False
        prec=m
    return c

#O(n²)

#-------------------------------------------------------------------------------------------------------------------------------

#Méthode de l'Arbre Couvrant Minimal et Algorithme de Christofides

#Méthode de l'Arbre Couvrant Minimal

def Prim(G):
    #G représente la matrice d'adjacence du graphe
    #Renvoie la liste des arêtes (sous la forme de couples de sommets) constituant un ACM de G
    n=len(G)
    s=n-1               #le sommet de départ sera n-1
    d=[float('inf')]*n
    d[n-1]=0
    p=[i for i in range(n)]
    H=[False]*n
    E=[]
    F=[False]*n
    F[n-1]=True
    for i in range(n):
        #retire de F un sommet u vérifiant d[u] minimal parmi les sommets de F
        u=0
        while not(F[u]):    #trouve le premier sommet dans F pour initialiser l'indice du min
            u+=1
        for j in range(u+1,n):
            if F[j] and d[j]<d[u]:
                u=j
        F[u]=False
        #
        if u!=s:
            E.append((u,p[u]))
        for v in range(n):
            if v!=u and not(F[v]) and not(H[v]):
                F[v]=True
            if v!=u and d[v]>G[u][v]:
                d[v]=G[u][v]
                p[v]=u
        H[u]=True
    return E

#O(n²)

def primtoliste(G):
    #G représente la matrice d'adjacence du graphe
    #Renvoie l'ACM trouvé par l'algorithme de Prim sous forme de liste d'adjacence
    n=len(G)
    L=[[]for i in range(n)]
    acm=prim(G)
    for x in acm:
        a,b=x
        L[a].append(b)
        L[b].append(a)
    return L

#O(n²)

#Implémentation du parcours en profondeur servant à l'étape 2 de l'algorithme de l'ACM

def pp(L,deja_vu,enum,u):
    #L sous forme de liste d'adjacence
    #deja_vu est un tableau de booléen avec les sommets déjà explorés marqués avec True
    #enum contient l'énumération en cours
    #u est le sommet dont on commence l'exploration
    deja_vu[u]=True
    enum.append(u)
    for v in L[u]:
        if deja_vu[v]==False:
            pp(L,deja_vu,enum,v)
    enum.append(u)

def Parcours_profondeur(L):
    #L graphe connexe sous forme de liste d'adjacence
    #Renvoie l'énumération des sommets correspondant au parcours en profondeur débutant au sommet 0
    n=len(L)
    deja_vu=[False]*n
    enum=[]
    pp(L,deja_vu,enum,0)
    return enum

#O(|V|+|E|)

def sol_tsp_acm(G):
    #G représente la matrice d'adjacence du graphe
    #Renvoie le cycle hamiltonien obtenu par la méthode de l'ACM
    P=parcours_profondeur(primtoliste(G))
    C=[]
    for i in range(len(P)):
        if P[i] not in C:
            C.append(P[i])
    return C

#O(n²)

#Algorithme de Christofides

#Implémentation de l'algorithme de Christofides, réutilisant la fonction primtoliste précédente correspondant à l'étape 1

#Etape 2
def sommet_impair(L):
    #Prend en paramètre un graphe donné sous forme de liste d'adjacence
    #Renvoie la liste des sommets de degré impair
    n=len(L)
    S=[]
    for i in range(n):
        if (len(L[i])%2)==1:
            S.append(i)
    return S

#O(n)

#Pour l'algorithme du couplage glouton, il est nécessaire de trier une liste selon la deuxième composante des éléments. Implémentons donc un tri fusion.

def fusion_comp2(L1,L2):
    #fonction auxiliaire
    #L1 et L2 sont deux listes triées selon la deuxième composante des éléments
    #Renvoie une liste triée qui correspond à la fusion des deux listes passées en paramètre
    L=[]
    i1=0
    i2=0
    n1=len(L1)
    n2=len(L2)
    for i in range(n1+n2):
        if i2==n2 or i1<n1 and L1[i1][1]<=L2[i2][1]:
            L.append(L1[i1])
            i1+=1
        else:
            L.append(L2[i2])
            i2+=1
    return L

def tri_fusion_comp2(L):
    #Trie la liste passée en paramètre selon la deuxième composante des éléments
    if len(L)<=1:
        return L[:]
    else:
        m=len(L)//2
        return fusion_comp2(tri_fusion_comp2(L[:m]),tri_fusion_comp2(L[m:]))

#O(k*log(k)) avec k le nombre d'éléments de la liste passée en paramètre

def couplage_glouton_graphe_induit(G):
    #G représente la matrice d'adjacence du graphe
    #Renvoie le couplage glouton du graphe induit par les sommets de degré impair
    L=primtoliste(G)
    n=len(L)
    S=sommet_impair(L)
    ni=len(S)
    Lt=[]      #construction de la liste associée au graphe induit par les sommets de S contenant les couples (arête, poids)
    for i in range(ni):           #construction en O(ni²)=O(n²)
        for j in range(i+1,ni):
            Lt.append(((S[i],S[j]),G[S[i]][S[j]]))
    Ltri=tri_fusion_comp2(Lt)
    deja_vu=[False]*n    #tableau permettant d'indiquer les sommets appartenant à une arête du couplage en cours (les sommets qui n'étaient pas de degré impair gardent la valeur False)
    ntri=len(Ltri)
    C=[]
    for i in range(ntri):            #O(ntri)=O(n²)
        a,b=Ltri[i][0]
        if not(deja_vu[a]) and not(deja_vu[b]):
            deja_vu[a]=True
            deja_vu[b]=True
            C.append(Ltri[i][0])
    return C

#O(n²log(n))

#Etape 3
def liste_adj_ACM_U_C(G):
    #G représente la matrice d'adjacence du graphe
    #Renvoie la liste d'adjacence représentant le multigraphe de l'union de l'ACM et du couplage trouvé
    L=primtoliste(G)
    C=couplage_glouton_graphe_induit(G)
    m=len(C)
    for i in range(m):
        a,b=C[i]
        L[a].append(b)
        L[b].append(a)
    return L

#O(n²log(n))

#Etape 4
def Euler(L,x):
    #Fonction récursive prenant en entrée un multigraphe sous forme de liste d'adjacence dont tous les sommets sont de degré pair et un sommet x et renvoyant un cycle eulérien
    P=[x]
    if L[x]==[]:    #cas de base : x est isolé
        return P
    else:
        y=x
        while L[y]!=[]:
            z=L[y].pop()
            L[z].remove(y)   #suppression de l'arête (y,z)
            y=z
            P.append(y)
        k=len(P)
        CE=[]
        for i in range(k):
            CE=CE+euler(L,P[i])
        return CE

#O(|V|+|E|)

#Etape 5
def sol_tsp_christofides(G):
    #G représente la matrice d'adjacence du graphe
    #Renvoie le cycle hamiltonien obtenu par l'algorithme de Christofides
    Ceuler=euler(liste_adj_ACM_U_C(G),0)
    n=len(G)
    S=[]
    deja_vu=[False]*n
    i=0
    while len(S)!=n:                    #enlève les sommets apparaissant deux fois
        if not(deja_vu[Ceuler[i]]):
            deja_vu[Ceuler[i]]=True
            S.append(Ceuler[i])
        i+=1
    return S

#O(n²log(n))

#-------------------------------------------------------------------------------------------------------------------------------

#Tracés

from random import random
import matplotlib.pyplot as plt

def creation_sommets(n):
    #Renvoie sous la forme d'une liste de coordonnées un ensemble de n points disposés aléatoirement dans le plan
    return [(random(), random()) for _ in range(n)]

def trace_chemin(P,C):
    #Prend en paramètre un ensemble de points sous la forme d'une liste de coordonnées et un chemin sous la forme renvoyée par les algorithmes
    #Trace le chemin en pointillé
    C.append(0)
    plt.axis("equal")
    Xp=[p[0] for p in P]
    Yp=[p[1] for p in P]
    plt.plot(Xp,Yp,'ko')
    Xt=[P[i][0] for i in C]
    Yt=[P[i][1] for i in C]
    plt.plot(Xt,Yt,'g--')
    plt.show()

def trace_chemin_naif():
    #Trace le chemin obtenu par l'algorithme naïf pour un ensemble de 10 points aléatoires
    P=creation_sommets(10)
    G=calcule_distances(P)
    C=sol_tsp_algo_naif(G)
    trace_chemin(P,C)

def trace_chemin_prog_dyn():
    #Trace le chemin obtenu par l'algorithme d'Held et Karp pour un ensemble de 10 points aléatoires
    P=creation_sommets(10)
    G=calcule_distances(P)
    C=sol_tsp_prog_dyn(G)
    trace_chemin(P,C)

def trace_chemins_exact_et_plus_proche_voisin():
    #Trace les chemins obtenus par l'algorithme naïf (en pointillé vert) et l'algorithme du plus proche voisin (en pointillé rouge) pour un ensemble de 10 points aléatoires
    P=creation_sommets(10)
    G=calcule_distances(P)
    Cn=sol_tsp_algo_naif(G)
    Cv=sol_tsp_algo_plus_proche_voisin(G)
    Cn.append(0)
    Cv.append(0)
    plt.axis("equal")
    Xp=[p[0] for p in P]
    Yp=[p[1] for p in P]
    plt.plot(Xp,Yp,'ko')
    Xtn=[P[i][0] for i in Cn]
    Ytn=[P[i][1] for i in Cn]
    plt.plot(Xtn,Ytn,'g--')
    Xtv=[P[i][0] for i in Cv]
    Ytv=[P[i][1] for i in Cv]
    plt.plot(Xtv,Ytv,'r--')
    plt.show()
